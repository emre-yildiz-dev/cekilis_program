import 'package:flutter/material.dart';

class Person {
  int id;
  String fullName;
  Color color;
  IconData personIconData;
  IconData deleteIconData;

  Person({
    required this.id,
    required this.fullName,
    required this.color,
    required this.personIconData,
    required this.deleteIconData,
  });
}
