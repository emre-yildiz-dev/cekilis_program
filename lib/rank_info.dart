import 'package:flutter/material.dart';
import 'package:line_icons/line_icon.dart';

class RankInfo extends StatelessWidget {
  final IconData iconData;
  final Color color;
  final double size;
  const RankInfo(
      {Key? key, required this.iconData, required this.color, this.size = 25})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LineIcon(
      iconData,
      color: color,
      size: size,
    );
  }
}
