import 'package:cekilis_programi_dosyalari/person.dart';
import 'package:flutter/material.dart';

class TileWidget extends ListTile {
  final int id;
  final Person person;
  final Widget trailingWidget;

  const TileWidget({
    super.key,
    required this.id,
    required this.person,
    required this.trailingWidget,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(person.personIconData),
      trailing: trailingWidget,
      title: Text(
        person.fullName,
      ),
      tileColor: person.color,
      hoverColor: Colors.black12,
      textColor: Colors.white,
    );
  }
}
