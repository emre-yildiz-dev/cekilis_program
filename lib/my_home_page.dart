import 'dart:math';

import 'package:cekilis_programi_dosyalari/delete_button.dart';
import 'package:cekilis_programi_dosyalari/input_widget.dart';
import 'package:cekilis_programi_dosyalari/tile_widget.dart';
import 'package:cekilis_programi_dosyalari/person.dart';
import 'package:cekilis_programi_dosyalari/rank_info.dart';
import 'package:cekilis_programi_dosyalari/winner_cup.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<TileWidget> _winnerTiles = [];
  List<TileWidget> _listTiles = [];

  final List<Color> _winnerTileColors = [
    Colors.redAccent,
    Colors.black,
    Colors.deepPurple,
  ];
  final List<Color> _listTileColors = [
    Colors.greenAccent,
    Colors.black26,
    Colors.blue,
    Colors.lightGreen,
    Colors.blueGrey,
    Colors.purpleAccent,
    Colors.orange,
    Colors.lime,
    Colors.cyanAccent,
  ];

  late Widget _topWidget;
  TextEditingController fullNameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _topWidget =
        InputWidget(controller: fullNameController, onTaped: addListTile);
  }

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title:  const Center(child: Text('Çekiliş Programı')),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: SizedBox(
                  width: deviceWidth > 600 ? 500 : deviceWidth - 100,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        children: [
                          Expanded(child: _topWidget),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      ListView.builder(
                          shrinkWrap: true,
                          itemCount: _winnerTiles.length,
                          itemBuilder: _winnerItemBuilder),
                      const SizedBox(
                        height: 40,
                      ),
                      Container(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: const Center(
                            child: Text(
                          "Kişiler Listesi",
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.bold),
                        )),
                      ),
                      ListView.builder(
                          shrinkWrap: true,
                          itemCount: _listTiles.length,
                          itemBuilder: _itemBuilder),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 56,
        margin: const EdgeInsets.symmetric(vertical: 24, horizontal: 12),
        child: Row(
          children: <Widget>[
            Expanded(
              child: GestureDetector(
                onTap: chooseWinner,
                child: Container(
                  alignment: Alignment.center,
                  color: Colors.blueAccent,
                  child: const Text("KAZANANI BELİRLE",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: Colors.white)),
                ),
              ),
            ),
            GestureDetector(
              onDoubleTap: reset,
              child: Container(
                width: 66,
                color: Colors.red,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    Icon(Icons.restart_alt, color: Colors.white),
                    Text("SIFIRLA", style: TextStyle(color: Colors.white))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void chooseWinner() {
    if (_listTiles.isNotEmpty) {
      _topWidget = const WinnerCup();
      int winnerNumber = Random().nextInt(_listTiles.length);
      TileWidget winner = _listTiles[winnerNumber];
      winner.person.color =
          _winnerTileColors[_winnerTiles.length % _winnerTileColors.length];
      _listTiles.remove(winner);
      _winnerTiles.add(winner);

      _winnerTiles = _winnerTiles
          .map((tile) => _updateListTile(
              tile,
              const RankInfo(
                iconData: Icons.emoji_emotions,
                color: Colors.greenAccent,
                size: 40.0,
              )))
          .toList();
      _listTiles = _listTiles
          .map((tile) => _updateListTile(
              tile,
              const RankInfo(
                iconData: LineIcons.frowningFaceAlt,
                color: Colors.redAccent,
              )))
          .toList();
      setState(() {});
    }
  }

  void reset()async {
    await addSweepstakeToCloud();
    _winnerTiles = [];
    _listTiles = [];
    _topWidget =
        InputWidget(controller: fullNameController, onTaped: addListTile);
    setState(() {});
  }

  ListTile addPerson() {
    return const ListTile();
  }

  void addListTile() {
    Person person = Person(
        id: _listTiles.length,
        fullName: fullNameController.text,
        personIconData: Icons.person,
        deleteIconData: Icons.delete,
        color: _listTileColors[_listTiles.length % _listTileColors.length]);
    if (person.fullName.isNotEmpty) {
      _listTiles.add(
        _buildListTile(person),
      );
      setState(() {});
    }
    fullNameController.clear();
  }

  Widget? _itemBuilder(BuildContext context, int index) {
    List<ListTile> list = _listTiles;
    return list[index];
  }

  Widget? _winnerItemBuilder(BuildContext context, int index) {
    List<ListTile> list = _winnerTiles;
    return list[index];
  }

  TileWidget _buildListTile(Person person) {
    return TileWidget(
      id: person.id,
      person: person,
      trailingWidget: DeleteButton(
        onDeleted: () {
          deletePerson(person.id);
        },
      ),
    );
  }

  TileWidget _updateListTile(TileWidget tileWidget, Widget newTrailingWidget) {
    return TileWidget(
      id: tileWidget.id,
      person: tileWidget.person,
      trailingWidget:
          SizedBox(width: 40.0, child: Center(child: newTrailingWidget)),
    );
  }

/*  Future addSweepstakeToCloud() async {
    Map<String, dynamic> data = {
      'user_email': 'ali@test.com',
      'date': DateTime.now(),
    };
    CollectionReference sweepstakes = FirebaseFirestore.instance.collection('sweepstakes');
    var result = await sweepstakes.add(data);
    await addMultipleCollection(result.id);
  }*/
  Future addSweepstakeToCloud() async {
  /*  List<String> part = [];
    for (var tile in _listTiles){
      part.add(tile.person.fullName);
    }*/
    var mainList = List<TileWidget>.from([..._winnerTiles, ..._listTiles]);
    var participants = mainList.map((element) => element.person.fullName).toList();
    var winners = _winnerTiles.map((element) => element.person.fullName).toList();

    Map<String, dynamic> data = {
      'user_email': 'emre@test.com',
      'date': DateTime.now(),
      'participants': participants ,
      'winners': winners,
    };
    CollectionReference sweepstakes = FirebaseFirestore.instance.collection('sweepstakes');
    var result = await sweepstakes.add(data);
  }

  deletePerson(int id) {
    _listTiles.removeWhere((element) => element.id == id);

    for (var element in _listTiles) {
      element.person.color = _listTileColors[_listTiles.indexOf(element)];
    }
    setState(() {});
  }

/*  Future addMultipleCollection(String id) async{
    CollectionReference sweepstakes = FirebaseFirestore.instance.collection('sweepstakes');
    Map<String, String> participant1 = {
     'name': 'Kenan'
    };
    Map<String, String> participant2 = {
      'name': 'Kismet'
    };
    Map<String, String> winner = {
      'name': 'Kenan'
    };
    await sweepstakes.doc(id).collection('participants').add(participant1);
    await sweepstakes.doc(id).collection('participants').add(participant2);
    await sweepstakes.doc(id).collection('winners').add(winner);
  }*/
}

//   _winnerList   [ ]
//   _listTiles  [ Emre, Murat, Kenan, Ezgi, Musa]
//   _mainList  [ Emre, Murat, Kenan, Ezgi]

//   _winnerList  [Murat]
//   _listTiles  [ Emre, Kenan, Ezgi]
//   _listTiles  [Murat, Emre, Kenan, Ezgi]

//   _winnerList  [Murat, Ezgi]
//   _listTiles  [ Emre, Kenan]
//  _mainList [Murat, Ezgi, Emre, Kenan]

// Set[beyaz, siyah, mavi, kirmizi, yesil]
// Set[beyaz]

// sweepstakes             // users
//  cekilis1 - 11.03        //    email:emre@test.com
                             //     fullname:
//      date                //    ali@test.com
//      user_email
//      participants
//          name
//          name
//          name
//          name
//          name
//      winners
//          name
//          name



//  cekilis2 - 15.03.2023    // emre@test.com
//  cekilis3 16.03.2023    // ali@test.com@test.com
