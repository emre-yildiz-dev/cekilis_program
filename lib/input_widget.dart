import 'package:flutter/material.dart';

class InputWidget extends StatelessWidget {
  final TextEditingController controller;
  final VoidCallback onTaped;

  const InputWidget(
      {super.key, required this.controller, required this.onTaped});

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(
          suffixIcon: Align(
            widthFactor: 1.0,
            heightFactor: 1.0,
            child: GestureDetector(
              onTap: onTaped,
              child: const Icon(
                Icons.add_box,
                size: 35,
                color: Colors.blueAccent,
              ),
            ),
          ),
          border: const OutlineInputBorder(),
          labelText: 'Ad Soyad'),
    );
  }
}
