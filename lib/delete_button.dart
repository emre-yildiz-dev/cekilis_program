import 'package:flutter/material.dart';

class DeleteButton extends StatelessWidget {
  final VoidCallback onDeleted;

  const DeleteButton({
    Key? key,
    required this.onDeleted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onDeleted();
      },
      child: const Icon(
        Icons.delete,
        color: Colors.redAccent,
      ),
    );
  }
}
