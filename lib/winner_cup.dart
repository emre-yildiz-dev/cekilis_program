import 'package:flutter/material.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';

class WinnerCup extends StatelessWidget {
  const WinnerCup({super.key});

  @override
  Widget build(BuildContext context) {
    return LineIcon(
      LineIcons.trophy,
      size: 80,
      color: Colors.blueAccent,
    );
  }
}
